<?php

namespace App\Entity;

use App\Repository\CryptoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Crypto
 *
 * @ORM\Table(name="crypto", indexes={@ORM\Index(name="FK_users", columns={"FK_users"})})
 * @ORM\Entity(repositoryClass: CryptoRepository::class)]
 *
 */
class Crypto
{
    /**
     * @var string
     *
     * @ORM\Column(name="list_id", type="string", length=8, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $listId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_currency", type="string", length=8, nullable=true)
     */
    private $listCurrency;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_symbol", type="string", length=5, nullable=true)
     */
    private $listSymbol;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_name", type="string", length=22, nullable=true)
     */
    private $listName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_logo_url", type="string", length=83, nullable=true)
     */
    private $listLogoUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_status", type="string", length=6, nullable=true)
     */
    private $listStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_price", type="decimal", precision=17, scale=12, nullable=true)
     */
    private $listPrice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_price_date", type="string", length=20, nullable=true)
     */
    private $listPriceDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_price_timestamp", type="string", length=20, nullable=true)
     */
    private $listPriceTimestamp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_circulating_supply", type="bigint", nullable=true)
     */
    private $listCirculatingSupply;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_max_supply", type="bigint", nullable=true)
     */
    private $listMaxSupply;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_market_cap", type="bigint", nullable=true)
     */
    private $listMarketCap;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_market_cap_dominance", type="decimal", precision=5, scale=4, nullable=true)
     */
    private $listMarketCapDominance;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_num_exchanges", type="integer", nullable=true)
     */
    private $listNumExchanges;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_num_pairs", type="integer", nullable=true)
     */
    private $listNumPairs;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_num_pairs_unmapped", type="integer", nullable=true)
     */
    private $listNumPairsUnmapped;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_first_candle", type="string", length=20, nullable=true)
     */
    private $listFirstCandle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_first_trade", type="string", length=20, nullable=true)
     */
    private $listFirstTrade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_first_order_book", type="string", length=20, nullable=true)
     */
    private $listFirstOrderBook;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_rank", type="integer", nullable=true)
     */
    private $listRank;

    /**
     * @var int|null
     *
     * @ORM\Column(name="list_rank_delta", type="integer", nullable=true)
     */
    private $listRankDelta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_high", type="decimal", precision=17, scale=12, nullable=true)
     */
    private $listHigh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_high_timestamp", type="string", length=20, nullable=true)
     */
    private $listHighTimestamp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_volume", type="decimal", precision=13, scale=2, nullable=true)
     */
    private $list1dVolume;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_price_change", type="decimal", precision=17, scale=14, nullable=true)
     */
    private $list1dPriceChange;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_price_change_pct", type="decimal", precision=5, scale=4, nullable=true)
     */
    private $list1dPriceChangePct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_volume_change", type="decimal", precision=12, scale=2, nullable=true)
     */
    private $list1dVolumeChange;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_volume_change_pct", type="decimal", precision=5, scale=4, nullable=true)
     */
    private $list1dVolumeChangePct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_market_cap_change", type="decimal", precision=13, scale=2, nullable=true)
     */
    private $list1dMarketCapChange;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_1d_market_cap_change_pct", type="decimal", precision=5, scale=4, nullable=true)
     */
    private $list1dMarketCapChangePct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_platform_currency", type="string", length=5, nullable=true)
     */
    private $listPlatformCurrency;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_first_priced_at", type="string", length=27, nullable=true)
     */
    private $listFirstPricedAt;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FK_users", referencedColumnName="id")
     * })
     */
    private $fkUsers;

    public function getListId(): ?string
    {
        return $this->listId;
    }

    public function getListCurrency(): ?string
    {
        return $this->listCurrency;
    }

    public function setListCurrency(?string $listCurrency): self
    {
        $this->listCurrency = $listCurrency;

        return $this;
    }

    public function getListSymbol(): ?string
    {
        return $this->listSymbol;
    }

    public function setListSymbol(?string $listSymbol): self
    {
        $this->listSymbol = $listSymbol;

        return $this;
    }

    public function getListName(): ?string
    {
        return $this->listName;
    }

    public function setListName(?string $listName): self
    {
        $this->listName = $listName;

        return $this;
    }

    public function getListLogoUrl(): ?string
    {
        return $this->listLogoUrl;
    }

    public function setListLogoUrl(?string $listLogoUrl): self
    {
        $this->listLogoUrl = $listLogoUrl;

        return $this;
    }

    public function getListStatus(): ?string
    {
        return $this->listStatus;
    }

    public function setListStatus(?string $listStatus): self
    {
        $this->listStatus = $listStatus;

        return $this;
    }

    public function getListPrice(): ?string
    {
        return $this->listPrice;
    }

    public function setListPrice(?string $listPrice): self
    {
        $this->listPrice = $listPrice;

        return $this;
    }

    public function getListPriceDate(): ?string
    {
        return $this->listPriceDate;
    }

    public function setListPriceDate(?string $listPriceDate): self
    {
        $this->listPriceDate = $listPriceDate;

        return $this;
    }

    public function getListPriceTimestamp(): ?string
    {
        return $this->listPriceTimestamp;
    }

    public function setListPriceTimestamp(?string $listPriceTimestamp): self
    {
        $this->listPriceTimestamp = $listPriceTimestamp;

        return $this;
    }

    public function getListCirculatingSupply(): ?string
    {
        return $this->listCirculatingSupply;
    }

    public function setListCirculatingSupply(?string $listCirculatingSupply): self
    {
        $this->listCirculatingSupply = $listCirculatingSupply;

        return $this;
    }

    public function getListMaxSupply(): ?string
    {
        return $this->listMaxSupply;
    }

    public function setListMaxSupply(?string $listMaxSupply): self
    {
        $this->listMaxSupply = $listMaxSupply;

        return $this;
    }

    public function getListMarketCap(): ?string
    {
        return $this->listMarketCap;
    }

    public function setListMarketCap(?string $listMarketCap): self
    {
        $this->listMarketCap = $listMarketCap;

        return $this;
    }

    public function getListMarketCapDominance(): ?string
    {
        return $this->listMarketCapDominance;
    }

    public function setListMarketCapDominance(?string $listMarketCapDominance): self
    {
        $this->listMarketCapDominance = $listMarketCapDominance;

        return $this;
    }

    public function getListNumExchanges(): ?int
    {
        return $this->listNumExchanges;
    }

    public function setListNumExchanges(?int $listNumExchanges): self
    {
        $this->listNumExchanges = $listNumExchanges;

        return $this;
    }

    public function getListNumPairs(): ?int
    {
        return $this->listNumPairs;
    }

    public function setListNumPairs(?int $listNumPairs): self
    {
        $this->listNumPairs = $listNumPairs;

        return $this;
    }

    public function getListNumPairsUnmapped(): ?int
    {
        return $this->listNumPairsUnmapped;
    }

    public function setListNumPairsUnmapped(?int $listNumPairsUnmapped): self
    {
        $this->listNumPairsUnmapped = $listNumPairsUnmapped;

        return $this;
    }

    public function getListFirstCandle(): ?string
    {
        return $this->listFirstCandle;
    }

    public function setListFirstCandle(?string $listFirstCandle): self
    {
        $this->listFirstCandle = $listFirstCandle;

        return $this;
    }

    public function getListFirstTrade(): ?string
    {
        return $this->listFirstTrade;
    }

    public function setListFirstTrade(?string $listFirstTrade): self
    {
        $this->listFirstTrade = $listFirstTrade;

        return $this;
    }

    public function getListFirstOrderBook(): ?string
    {
        return $this->listFirstOrderBook;
    }

    public function setListFirstOrderBook(?string $listFirstOrderBook): self
    {
        $this->listFirstOrderBook = $listFirstOrderBook;

        return $this;
    }

    public function getListRank(): ?int
    {
        return $this->listRank;
    }

    public function setListRank(?int $listRank): self
    {
        $this->listRank = $listRank;

        return $this;
    }

    public function getListRankDelta(): ?int
    {
        return $this->listRankDelta;
    }

    public function setListRankDelta(?int $listRankDelta): self
    {
        $this->listRankDelta = $listRankDelta;

        return $this;
    }

    public function getListHigh(): ?string
    {
        return $this->listHigh;
    }

    public function setListHigh(?string $listHigh): self
    {
        $this->listHigh = $listHigh;

        return $this;
    }

    public function getListHighTimestamp(): ?string
    {
        return $this->listHighTimestamp;
    }

    public function setListHighTimestamp(?string $listHighTimestamp): self
    {
        $this->listHighTimestamp = $listHighTimestamp;

        return $this;
    }

    public function getList1dVolume(): ?string
    {
        return $this->list1dVolume;
    }

    public function setList1dVolume(?string $list1dVolume): self
    {
        $this->list1dVolume = $list1dVolume;

        return $this;
    }

    public function getList1dPriceChange(): ?string
    {
        return $this->list1dPriceChange;
    }

    public function setList1dPriceChange(?string $list1dPriceChange): self
    {
        $this->list1dPriceChange = $list1dPriceChange;

        return $this;
    }

    public function getList1dPriceChangePct(): ?string
    {
        return $this->list1dPriceChangePct;
    }

    public function setList1dPriceChangePct(?string $list1dPriceChangePct): self
    {
        $this->list1dPriceChangePct = $list1dPriceChangePct;

        return $this;
    }

    public function getList1dVolumeChange(): ?string
    {
        return $this->list1dVolumeChange;
    }

    public function setList1dVolumeChange(?string $list1dVolumeChange): self
    {
        $this->list1dVolumeChange = $list1dVolumeChange;

        return $this;
    }

    public function getList1dVolumeChangePct(): ?string
    {
        return $this->list1dVolumeChangePct;
    }

    public function setList1dVolumeChangePct(?string $list1dVolumeChangePct): self
    {
        $this->list1dVolumeChangePct = $list1dVolumeChangePct;

        return $this;
    }

    public function getList1dMarketCapChange(): ?string
    {
        return $this->list1dMarketCapChange;
    }

    public function setList1dMarketCapChange(?string $list1dMarketCapChange): self
    {
        $this->list1dMarketCapChange = $list1dMarketCapChange;

        return $this;
    }

    public function getList1dMarketCapChangePct(): ?string
    {
        return $this->list1dMarketCapChangePct;
    }

    public function setList1dMarketCapChangePct(?string $list1dMarketCapChangePct): self
    {
        $this->list1dMarketCapChangePct = $list1dMarketCapChangePct;

        return $this;
    }

    public function getListPlatformCurrency(): ?string
    {
        return $this->listPlatformCurrency;
    }

    public function setListPlatformCurrency(?string $listPlatformCurrency): self
    {
        $this->listPlatformCurrency = $listPlatformCurrency;

        return $this;
    }

    public function getListFirstPricedAt(): ?string
    {
        return $this->listFirstPricedAt;
    }

    public function setListFirstPricedAt(?string $listFirstPricedAt): self
    {
        $this->listFirstPricedAt = $listFirstPricedAt;

        return $this;
    }

    public function getFkUsers(): ?Users
    {
        return $this->fkUsers;
    }

    public function setFkUsers(?Users $fkUsers): self
    {
        $this->fkUsers = $fkUsers;

        return $this;
    }


}
