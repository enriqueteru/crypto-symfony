<?php

namespace App\Controller;

use App\Entity\Crypto;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CryptoController extends AbstractController
{

    #[Route('/
    ')]
    public function showcryptoCurrencies(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Crypto::class);
        $cryptoCurrencies = $repo->findAll();

        return $this->render('crypto/showCrypto.html.twig', [ "crypto" => $cryptoCurrencies]);
    }
}
