<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309122800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE crypto CHANGE list_id list_id VARCHAR(8) NOT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_currency list_currency VARCHAR(8) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_symbol list_symbol VARCHAR(5) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_name list_name VARCHAR(22) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_logo_url list_logo_url VARCHAR(83) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_status list_status VARCHAR(6) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_price_date list_price_date VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_price_timestamp list_price_timestamp VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_first_candle list_first_candle VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_first_trade list_first_trade VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_first_order_book list_first_order_book VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_high_timestamp list_high_timestamp VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_platform_currency list_platform_currency VARCHAR(5) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE list_first_priced_at list_first_priced_at VARCHAR(27) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`');
        $this->addSql('ALTER TABLE users CHANGE role role VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE name name VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE subname subname VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE email email VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`, CHANGE password password VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_0900_ai_ci`');
    }
}
